# Camera Frame Image Maker 
Make camera frame image for presentation.

***Example gif***

![Gif](image/example.gif)

***Result example(with transparent background)***

![Result](image/result-example.png)

## Dependencies
Depends on python and openCV.

## Usage
```bash
python camera_frame_image_maker.py [image(option)]
```

- Drag mouse to change camera angle.
- Select color from bottom color palette.

### Save image
Press s key to save as png image
