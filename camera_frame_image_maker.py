import sys
import os
import cv2
import numpy as np
import math
import datetime

def mouse_cb(event,x,y,flags,param):
    global ix,iy,dragging,cur_event,cur_x,cur_y

    if event == cv2.EVENT_LBUTTONDOWN:
        cur_event = cv2.EVENT_LBUTTONDOWN
        dragging = True
        ix,iy = x,y

    elif event == cv2.EVENT_MOUSEMOVE:
        cur_event = cv2.EVENT_MOUSEMOVE
        cur_x, cur_y = x, y

    elif event == cv2.EVENT_LBUTTONUP:
        cur_event = cv2.EVENT_LBUTTONUP
        dragging = False

def nothing(x):
    pass

def overlay(foreground, background):
    # Make mask
    mask = foreground[:,:,3]    # Extract alpha channel
    mask = cv2.cvtColor(mask, cv2.COLOR_GRAY2BGR)
    mask = mask / 255
    # Make mixed alpha channel
    mixed_alpha = np.logical_or((background[:,:,3]/255), (foreground[:,:,3]/255))
    mixed_alpha = mixed_alpha * 255
    mixed_alpha = mixed_alpha.astype(np.uint8)
    
    foreground = foreground[:,:,:3]    # Extract BGR cnannel
    background = background[:,:,:3]    # Extract BGR cnannel
    
    background *= 1 - mask
    background += foreground * mask
    b,g,r = cv2.split(background)
    background = cv2.merge((b,g,r,mixed_alpha))

    return background

def draw_frames(img, edges, color):
    thickness = cv2.getTrackbarPos('Frame thickness', 'Camera Frame Image Maker')
    img = cv2.line(img, tuple(edges[0]), tuple(edges[1]), color, thickness)
    img = cv2.line(img, tuple(edges[0]), tuple(edges[2]), color, thickness)
    img = cv2.line(img, tuple(edges[0]), tuple(edges[3]), color, thickness)
    img = cv2.line(img, tuple(edges[0]), tuple(edges[4]), color, thickness)
    img = cv2.line(img, tuple(edges[1]), tuple(edges[2]), color, thickness)
    img = cv2.line(img, tuple(edges[2]), tuple(edges[3]), color, thickness)
    img = cv2.line(img, tuple(edges[3]), tuple(edges[4]), color, thickness)
    img = cv2.line(img, tuple(edges[4]), tuple(edges[1]), color, thickness)

    return img

def draw_camera_frame(img, edges, color, rot_mat, use_camera_image, camera_image):
    # Check eye vector for drawing priority
    eye_vec = np.array([[0],[0],[1]])
    eye_vec = np.dot(rot_mat,eye_vec)

    # Draw camera frame
    img = draw_frames(img, edges, color)
    if use_camera_image:
        # Draw camera image
        camera_image = draw_camera_image(camera_image, edges, img.shape)

        if eye_vec[2] > 0:
            img = overlay(img, camera_image)
        else:
            img = overlay(camera_image, img)

    return img

def draw_camera_image(cam_img, edge, shape):
    h, w, c = cam_img.shape
    src_pts = np.array([[0, 0], [w, 0], [w, h], [0, h]], dtype=np.float32)
    dst_pts = np.array([edge[1], edge[2], edge[3], edge[4]], dtype=np.float32)
    mat = cv2.getPerspectiveTransform(src_pts, dst_pts)

    return cv2.warpPerspective(cam_img, mat, (shape[1], shape[0]), borderValue=(255,255,255,0))

def calc_rot_mat(yaw, pitch):
    cos_x = math.cos(math.radians(yaw))
    sin_x = math.sin(math.radians(yaw))
    cos_y = math.cos(math.radians(pitch))
    sin_y = math.sin(math.radians(pitch))
    R_x = np.array([[1.0,0.0,0.0],
                    [0.0,cos_x,-sin_x],
                    [0.0,sin_x,cos_x]])
    R_y = np.array([[cos_y,0.0,sin_y],
                    [0.0,1.0,0.0],
                    [-sin_y,0.0,cos_y]])
    return np.dot(R_y,R_x)

def project_frame_edges(edges_in_3d, camera_matrix, window_name, rot_mat):
    rvec, jacobian = cv2.Rodrigues(rot_mat)
    tvec = np.array([0.0,0.0,0.3])
    edges_on_image, jacobian = cv2.projectPoints(edges_in_3d, rvec, tvec, camera_matrix, np.array([]))
    edges_on_image = np.int32(edges_on_image).reshape(-1,2)

    return edges_on_image

def save_image(edges_on_image, color, rot_mat, use_camera_image, camera_image):
        # Get bounding box
        min_x = (edges_on_image*2)[:,1].min() - 2
        max_x = (edges_on_image*2)[:,1].max() + 2
        min_y = (edges_on_image*2)[:,0].min() - 2
        max_y = (edges_on_image*2)[:,0].max() + 2

        # Shift edges
        shifted_edges = edges_on_image[:]*2 - [min_y,min_x]

        save_img = np.full((max_x - min_x,max_y - min_y,4), 255, dtype=np.uint8) 
        save_img[:,:,3] = 0    # Alpha channel
        save_img = draw_camera_frame(save_img, shifted_edges, color, rot_mat, use_camera_image, camera_image)
        filename = 'camera_frame-'+datetime.datetime.now().strftime('%Y-%m-%d-%H-%m-%s')+'.png'
        cv2.imwrite(filename,save_img)
        print 'Saved image as ' + filename

def load_camera_image(image_path):
    camera_image = cv2.imread(image_path)
    if camera_image.shape[2] == 3:
        b,g,r = cv2.split(camera_image)
        a = np.full(b.shape, 255,dtype=b.dtype)
        camera_image = cv2.merge((b,g,r,a))
    elif camera_image.shape[2] == 1:
        camera_image = cv2.cvtColor(camera_image, cv2.COLOR_GRAY2BGR)

    return camera_image

def gui_window():
    global cur_event
    window_name = 'Camera Frame Image Maker'
    cv2.namedWindow(window_name)
    cv2.setMouseCallback(window_name, mouse_cb)
    cv2.createTrackbar('Frame thickness','Camera Frame Image Maker',2,100,nothing)
    # Load camera image
    use_camera_image = False
    camera_image = np.full((300,400,4), 150, dtype=np.uint8)
    args = sys.argv
    if len(args)>=2:
        use_camera_image = True
        image_path = os.path.normpath(os.path.join(os.getcwd(),args[1]))
        camera_image = load_camera_image(image_path)
    # Camera image aspect
    aspect_ratio_h = 1
    aspect_ratio_w = float(camera_image.shape[1])/float(camera_image.shape[0])
    focal_length = aspect_ratio_h
    aspect_ratio_h *= 0.1
    aspect_ratio_w *= 0.1
    focal_length *= 0.1
    # Set camera parameter
    width = height = f = 500
    camera_matrix = np.array([[f  , 0.0, width/2], 
                              [0.0, f  , height/2],
                              [0.0, 0.0, 1.0]])
    # Define Camera frame edges
    camera_frame_edge = np.array([[0,0,0],[-aspect_ratio_w/2.0,-aspect_ratio_h/2.0,0.1],[aspect_ratio_w/2.0,-aspect_ratio_h/2.0,0.1],[aspect_ratio_w/2.0,aspect_ratio_h/2.0,0.1],[-aspect_ratio_w/2.0,aspect_ratio_h/2.0,0.1]], dtype=np.float)
    edges_on_image = project_frame_edges(camera_frame_edge, camera_matrix, window_name, calc_rot_mat(0,0))
    frame_color = (0,0,255,255)
    # Yaw and Pitch
    yaw = prev_yaw = 0
    pitch =prev_pitch = 0
    # Prepare canvas
    img = np.full((width,height,4), 255, dtype=np.uint8) 
    img[0:height+1,0:width+1,3] = 0    # Alpha channel
    # Create colormap
    colormap = cv2.imread('image/colors.bmp')
    colormap = cv2.resize(colormap, (width, colormap.shape[0]))
    b,g,r = cv2.split(colormap)
    a = np.full(b.shape, 255,dtype=b.dtype)
    colormap = cv2.merge((b,g,r,a))
    img = np.concatenate([img,colormap])

    # Main loop
    while(1):
        # Process mouse event
        if cur_event == cv2.EVENT_MOUSEMOVE:
            if dragging and cur_y < height:    # if mouse is dragging
                pitch = prev_pitch + ix - cur_x
                yaw = prev_yaw + cur_y - iy
                # Project 3d edges to image plane
                edges_on_image = project_frame_edges(camera_frame_edge, camera_matrix, window_name, calc_rot_mat(yaw, pitch))
        elif  cur_event == cv2.EVENT_LBUTTONUP:    # if mouse left button is released
            prev_pitch = pitch
            prev_yaw = yaw
        elif cur_event == cv2.EVENT_LBUTTONDOWN and height < cur_y:    # Select frame color
            frame_color = tuple(int(x) for x in img[cur_y, cur_x])

        # Clear image
        img[0:height+1,0:width+1,0:3] = 255
        img[0:height+1,0:width+1,3] = 0    # Alpha channel
        # Draw camera frame
        img = draw_camera_frame(img[0:height,0:width], edges_on_image, frame_color, calc_rot_mat(yaw,pitch), use_camera_image, camera_image)
        img = np.concatenate([img,colormap])
        # Show image
        cv2.imshow(window_name, img)
        if cv2.waitKey(20) & 0xFF == 27:    # esc key
            break
        elif cv2.waitKey(20) & 0xFF == 115:    # s key
            save_image(edges_on_image, frame_color, calc_rot_mat(yaw, pitch), use_camera_image, camera_image)
            
    cv2.destroyAllWindows()

def main():
    gui_window()

if __name__ == '__main__':
    dragging = False # true if mouse is pressed
    ix,iy = -1,-1
    cur_x, cur_y = -1, -1
    cur_event = 0
    main()
